# RGBRemote (Aymen Bellakhder) 
### Project Overview: 
* This is an RGB Remote Controller App that uses the IR Blaster from an Android Device to control an RGB LED Strip (On/off, Switch Colors, Control Brightness...).
* This App Only works with android devices that have a built-in IR Emitter.

### Project Details:
* The app was designed using an MVVC Architecture. All the IR Pattern codes are stored in the **IRcodes.json** file with an index and their functionalities.
* The IR pattern, index, name from the JSON file are put then in the **fRemoteCodes** ArrayList.
* Each button in the UI  calls the **emitIR(int index**) method using the DataBinding Library  with an index as an argument to transmit the right IR pattern from the Arraylist **fRemoteCodes**. 
### Video of the App:
https://vimeo.com/388871136

### Screenshots:
![App Screenshot1](https://lh3.googleusercontent.com/3-jOXpVBWovpOndhJ0m5cJdqc4VvmGAdjHADds7ZCtEoAXa60YV333XbQ7aAS4UhuRUMtrdxJHdBv8avNLIq=w1920-h987)
![App Screenshot2](https://lh4.googleusercontent.com/QyXz5hvV1r46ebotd5-Mar004ixkLLCOKk-Nj1iIYc8ZCMCr016CmI2_JMa2WdOoZf4jYhcGY_-USG106CGW=w1920-h987)
